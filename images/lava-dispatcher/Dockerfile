ARG HUB=hub.lavasoftware.org/lava/lava/amd64/
ARG VERSION
FROM ${HUB}lava-dispatcher:${VERSION}

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update -qq && \
    apt-get install --no-install-recommends --yes git patch tftp && \
    apt-get clean

# Add lava-coordinator configuration
COPY lava-coordinator.conf /etc/lava-coordinator/lava-coordinator.conf

# Install DummySys
RUN git clone https://framagit.org/ivoire/DummySys /root/DummySys

# Patch pyudev monitor to always return the expected events
COPY patches/0001-pyudev-monitor.patch /tmp/0001-pyudev-monitor.patch
RUN patch -p0 < /tmp/0001-pyudev-monitor.patch && \
    rm /tmp/0001-pyudev-monitor.patch
COPY udev-events.yaml /root/udev-events.yaml
# Do not test the /dev directories as we can't fake it
RUN sed -i "s#not os.path.exists(device_path)#False#" /usr/lib/python3/dist-packages/lava_dispatcher/actions/deploy/vemsd.py

# Overwrite some binaries
COPY overwrites/dfu-util /root/bin/dfu-util
COPY overwrites/dpkg-query /root/bin/dpkg-query
COPY overwrites/mount /root/bin/mount
COPY overwrites/pyocd-flashtool /root/bin/pyocd-flashtool
COPY overwrites/umount /root/bin/umount
RUN ln -s /bin/true "/bin/same with spaces"

# entry point
COPY entrypoint.sh /root/
ENTRYPOINT ["/root/entrypoint.sh"]
