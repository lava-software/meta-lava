#!/bin/sh

set -e

echo "Upgrading to latest version"
(cd /root/DummySys && git pull --rebase)
echo "done"
echo

echo "Starting tftpd"
/etc/init.d/tftpd-hpa start
echo "done"
echo

echo "Starting command"
if [ -n "$1" ]
then
  exec "$@"
else
  exec bash
fi
